import React from 'react';
import ReactDOM from 'react-dom/client';
import { App } from './App';

import { CssBaseline, ThemeProvider } from '@mui/material';
import { Toaster } from 'react-hot-toast';
import './index.scss';
import { AuthProvider } from './providers/auth.provider';
import { theme } from './theme/theme';

ReactDOM.createRoot(document.getElementById('root')!).render(
  <React.StrictMode>
    <ThemeProvider theme={theme}>
      <CssBaseline />
      <AuthProvider>
        <App />
      </AuthProvider>
    </ThemeProvider>
    <Toaster />
  </React.StrictMode>
);
