import axios from 'axios';
import { CMS_ROOT } from '../constants';
import { AuthRequest, AuthResponse, RegisterRequest } from '../types/auth';

export const registerUser = (data: RegisterRequest) => {
  return axios
    .post<AuthResponse>(`${CMS_ROOT}/auth/local/register`, data)
    .then((r) => r.data);
};

export const authUser = (data: AuthRequest) => {
  return axios
    .post<AuthResponse>(`${CMS_ROOT}/auth/local`, data)
    .then((r) => r.data);
};
