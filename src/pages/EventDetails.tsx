import { ThumbUp } from '@mui/icons-material';
import {
  Box,
  CircularProgress,
  Container,
  IconButton,
  Typography,
  styled,
} from '@mui/material';
import { useEffect, useState } from 'react';
import toast from 'react-hot-toast';
import { useParams } from 'react-router-dom';
import { useAuth } from '../hooks/useAuth';
import { getEvent, voteForEvent } from '../services/events';
import { CmsEvent } from '../types/events';

export const EventDetails = () => {
  const { id } = useParams();
  const { userId } = useAuth();
  const [event, setEvent] = useState<CmsEvent>();

  const fetchData = (id: number) => {
    getEvent(id).then((r) => {
      setEvent(r.data);
    });
  };

  useEffect(() => {
    if (!id) {
      return;
    }
    fetchData(+id);
  }, [id]);

  const handleVote = async () => {
    if (!id) {
      return;
    }
    const res = await voteForEvent(+id);
    await fetchData(+id);
    if (res.data) {
      toast.success('Ваш голос учтён');
    }
  };

  if (!event) {
    return <CircularProgress />;
  }
  return (
    <Container>
      <Typography variant="h2">{event.attributes.title} </Typography>
      <Typography>Голосов: {event.attributes.votes ?? 0}</Typography>
      {!!userId && (
        <Box>
          <IconButton onClick={handleVote}>
            <ThumbUp />
          </IconButton>
        </Box>
      )}
      <Box mt={'24px'}>
        <ImageContainer fullWidth>
          <img src={event.attributes.cover.data.attributes.url} />
        </ImageContainer>
        <Typography whiteSpace={'pre-wrap'} mt={'14px'}>
          {event.attributes.description}
        </Typography>
      </Box>
    </Container>
  );
};

const ImageContainer = styled('div')<{
  fullWidth?: boolean;
}>`
  display: flex;
  justify-content: center;

  & > img {
    width: 100%;
    max-width: ${(props) => (props.fullWidth ? '100%' : '400px')};
  }
`;
