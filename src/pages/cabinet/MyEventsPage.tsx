import { EditNote } from '@mui/icons-material';
import {
  Box,
  Container,
  IconButton,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
} from '@mui/material';
import { useEffect, useState } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import { useAuth } from '../../hooks/useAuth';
import { getMyEvents } from '../../services/events';
import { CmsEvent } from '../../types/events';

export const MyEventsPage = () => {
  const [events, setEvents] = useState<CmsEvent[]>([]);
  const navigate = useNavigate();
  const { userId } = useAuth();

  useEffect(() => {
    getMyEvents(userId).then((r) => {
      setEvents(r.data);
    });
  }, [userId]);

  return (
    <Container>
      <Link to="/cabinet/events/create">Добавить мероприятие</Link>
      <Box mt={'18px'}>
        <Table sx={{ minWidth: 650 }} aria-label="simple table">
          <TableHead>
            <TableRow>
              <TableCell>ID</TableCell>
              <TableCell>Название</TableCell>
              <TableCell>Описание</TableCell>
              <TableCell></TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {events.map((row) => (
              <TableRow
                key={row.id}
                sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
              >
                <TableCell>{row.id}</TableCell>
                <TableCell>{row.attributes.title}</TableCell>
                <TableCell>{row.attributes.description}</TableCell>
                <TableCell>
                  <IconButton
                    onClick={() => {
                      navigate(`/cabinet/events/edit/${row.id}`);
                    }}
                  >
                    <EditNote />
                  </IconButton>
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </Box>
    </Container>
  );
};
