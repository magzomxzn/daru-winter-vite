import { Container } from '@mui/material';
import { useNavigate } from 'react-router-dom';
import { EventForm } from '../../components/events/EventForm';
import { useAuth } from '../../hooks/useAuth';
import { createEvent } from '../../services/events';
import { CreateEventRequest } from '../../types/events';

export const MyEventsCreatePage = () => {
  const navigate = useNavigate();

  const { userId } = useAuth();

  const handleSubmit = (data: CreateEventRequest) => {
    createEvent({ ...data, creator: userId }).then((r) => {
      if (r.data) {
        navigate('/cabinet/events');
      }
    });
  };

  return (
    <Container>
      <EventForm onSubmit={handleSubmit} />
    </Container>
  );
};
