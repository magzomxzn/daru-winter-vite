import { styled } from '@mui/material';

export const Nav = styled('nav')`
  margin: 0 24px;
  display: flex;
  gap: 14px;

  a {
    text-decoration: none;
    color: #fff;
    &:hover {
      text-decoration: underline;
    }
  }
`;
