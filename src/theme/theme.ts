import { createTheme } from '@mui/material';

export const theme = createTheme({
  typography: {
    h1: {
      fontSize: '32px',
      lineHeight: '36px',
      fontWeight: '700',
    },
  },
  components: {
    MuiAppBar: {
      styleOverrides: {
        root: {},
      },
    },
  },
});
