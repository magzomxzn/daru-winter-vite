import React from 'react';
import { RouterProvider, createBrowserRouter } from 'react-router-dom';
import './App.css';
import { AuthLayout } from './components/layout/AuthLayout';
import { MainLayout } from './components/layout/MainLayout';
import { CategoriesPage } from './pages/Categories';
import { EventDetails } from './pages/EventDetails';
import { EventsPage } from './pages/Events';
import { Index } from './pages/Index';
import { RegisterPage } from './pages/Register';
import { MyEventsCreatePage } from './pages/cabinet/MyEventsCreatePage';
import { MyEventsEditPage } from './pages/cabinet/MyEventsEditPage';
import { MyEventsPage } from './pages/cabinet/MyEventsPage';
import { LoginPage } from './pages/Login';

const router = createBrowserRouter([
  {
    path: '/',
    element: <MainLayout />,
    children: [
      {
        path: '/',
        element: <Index />,
      },
      {
        path: '/events/details/:id',
        element: <EventDetails />,
      },
      {
        path: '/events/:type',
        element: <EventsPage />,
      },
      {
        path: '/events/categories',
        element: <CategoriesPage />,
      },
      {
        path: '/cabinet/events',
        element: <MyEventsPage />,
      },
      {
        path: '/cabinet/events/create',
        element: <MyEventsCreatePage />,
      },
      {
        path: '/cabinet/events/edit/:id',
        element: <MyEventsEditPage />,
      },
    ],
  },
  {
    path: '/auth',
    element: <AuthLayout />,
    children: [
      {
        path: 'register',
        element: <RegisterPage />,
      },
      {
        path: 'login',
        element: <LoginPage />,
      },
    ],
  },
]);

export const App: React.FC = () => {
  return <RouterProvider router={router} />;
};
